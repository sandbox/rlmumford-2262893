<?php
/**
 * @file
 *  Administration code for OpenCRM Location Types.
 */

/**
 * Main OpenCRM Location Type Edit form.
 */
function opencrm_location_type_form($form, &$form_state, $type, $op = 'edit') {
  if ($op == 'clone') {
    $type->cloned_from = $type->type;
    $type->label .= ' (cloned)';
    $type->type = '';
  }

  $form['#op'] = $op;

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $type->label,
    '#description' => t('The human-readable name of this location type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($type->type) ? $type->type : '',
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'opencrm_location_type_load',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this location type. It must only contain lowercase letters, numbers, and underscores.'),
    '#disabled' => empty($type->is_new),
  );
  
  $form['reusable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Locations of this type are reusable'),
    '#description' => t('Reusable locations can be used multiple times but cannot be edited by the Location field widget.'),
    '#default_value' => FALSE,
  );

  $form['cloned_from'] = array(
    '#type' => 'value',
    '#value' => !empty($type->cloned_from) ? $type->cloned_from : NULL,
  );

  $form['clone_fields'] = array(
    '#type' => 'checkbox',
    '#title' => t('Copy Field Definitions'),
    '#description' => t('This will copy the field instance settings from the original type.'),
    '#default_value' => FALSE,
    '#access' => ($op == 'clone'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit callback for the type form.
 */
function opencrm_location_type_form_submit($form, &$form_state) {
  $type = entity_ui_form_submit_build_entity($form, $form_state);
  $type->save();

  // Copy fields.
  if (!empty($form_state['values']['clone_fields'])) {
    $original = $form_state['values']['cloned_from'];
    foreach (field_info_instances('opencrm_location', $original) as $instance) {
      $instance['bundle'] = $type->type;
      if (!field_info_instance('opencrm_location', $instance['field_name'], $type->type)) {
        field_create_instance($instance);
      }
    }
  }
}
