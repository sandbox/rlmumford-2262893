<?php
/**
 * @file
 *   The main file for code for the opencrm location module.
 */

/**
 * Implments hook_modules_enabled().
 */
function opencrm_location_modules_enabled($modules) {
  if (in_array('uuid', $modules)) {
    db_add_field('opencrm_location', 'uuid', uuid_schema_field_definition());
    db_add_index('opencrm_location', 'uuid', array('uuid'));
  }
}

/**
 * Implements hook_hook_info().
 */
function opencrm_location_hook_info() {
  $hooks['default_opencrm_location_type'] = array('group' => 'opencrm_location_type_default');
  $hooks['default_opencrm_location_type_alter'] = array('group' => 'opencrm_location_type_default');
  return $hooks;
}

/**
 * Implements hook_entity_info().
 */
function opencrm_location_entity_info() {
  $info = array();

  $info['opencrm_location'] = array(
    'label' => t('OpenCRM Location'),
    'base table' => 'opencrm_location',
    'entity class' => 'Entity',
    'controller class' => 'EntityAPIController',
    'metadata controller class' => 'OpenCRMLocationMetadataController',
    'fieldable' => TRUE,
    'module' => 'opencrm_location',
    'entity keys' => array(
      'id' => 'id',
      'label' => 'title',
      'bundle' => 'type',
    ),
    'bundle keys' => array(
      'bundle' => 'type',
    ),
    'bundles' => array(),
    'view modes' => array(
      'full' => array(
        'label' => t('Full Location'),
        'custom settings' => FALSE,
      ),
      'summary' => array(
        'label' => t('Summary'),
        'custom settings' => TRUE,
      ),
    ),
    'access callback' => 'opencrm_location_crud_access',
    'inline entity form' => array(
      'controller' => 'OpenCRMLocationInlineEntityFormController',
    ),
    // Enable the entity API's admin UI.
    'admin ui' => array(
      'path' => 'admin/content/location',
      'file' => 'opencrm_location.admin.inc',
      'controller class' => 'OpenCRMLocationUIController',
    ),
  );

  // Add the bundles to opencrm_location entity.
  $types = db_select('opencrm_location_type', 'et')
    ->fields('et', array('type', 'label'))
    ->execute()
    ->fetchAllKeyed();

  foreach ($types as $type => $label) {
    $info['opencrm_location']['bundles'][$type] = array(
      'label' => $label,
      'admin' => array(
        'path' => 'admin/structure/opencrm_location/manage/%opencrm_location_type',
        'real path' => 'admin/structure/opencrm_location/manage/' . $type,
        'bundle argument' => 4,
        'access arguments' => array('administer opencrm_location types'),
      ),
    );
  }

  // The entity that holds information about the entity types
  $info['opencrm_location_type'] = array(
    'label' => t('OpenCRM Location Type'),
    'entity class' => 'Entity',
    'controller class' => 'EntityAPIControllerExportable',
    'base table' => 'opencrm_location_type',
    'fieldable' => FALSE,
    'bundle of' => 'opencrm_location',
    'exportable' => TRUE,
    'access callback' => 'opencrm_location_type_access',
    'entity keys' => array(
      'id' => 'id',
      'name' => 'type',
      'label' => 'label',
    ),
    'module' => 'opencrm_location',
    // Enable the entity API's admin UI.
    'admin ui' => array(
      'path' => 'admin/structure/opencrm_location',
      'file' => 'opencrm_location_type.admin.inc',
      'controller class' => 'EntityDefaultUIController',
    ),
  );

  if (module_exists('uuid')) {
    $info['opencrm_location']['uuid'] = TRUE;
    $info['opencrm_location']['entity keys']['uuid'] = 'uuid';
  }

  return $info;
}

/**
 * Implements hook_permission().
 */
function opencrm_location_permission() {
  $perms = array();
  $perms['administer opencrm_location types'] = array(
    'title' => t('Administer Location Types'),
    'description' => t('Add, Remove and Manage Fields on Location Types.'),
  );

  // Crud Permissions.
  $perms['create opencrm_locations'] = array(
    'title' => t('Create new Locations'),
  );
  $perms['edit opencrm_locations'] = array(
    'title' => t('Edit Locations'),
  );
  $perms['use inactive locations'] = array(
    'title' => t('Use Inactive Locations'),
  );
  return $perms;
}

/**
 * Implements hook_views_api().
 */
function opencrm_location_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'opencrm_location') . '/views',
  );
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function opencrm_location_ctools_plugin_directory($owner, $plugin_type) {
  if ($owner == 'panelizer' && $plugin_type == 'entity') {
    return "plugins/$plugin_type";
  }
}

/**
 * Load a location.
 */
function opencrm_location_load($id) {
  return entity_load_single('opencrm_location', $id);
}

/**
 * Access manager for locations.
 */
function opencrm_location_crud_access($op, $entity, $user = NULL, $entity_type = 'opencrm_location') {
  if ($op == 'view') {
    return TRUE;
  }

  if ($op == 'create') {
    return !empty($entity->reusable) && user_access('create opencrm_locations', $user);
  }
  else {
    $type = isset($entity->type) ? entity_load_single('opencrm_location_type', $entity->type) : NULL;
    return (empty($type) || !empty($type->reusable)) && user_access('edit opencrm_locations', $user);
  }
}

/**
 * Load an OpenCRM Location type.
 */
function opencrm_location_type_load($id) {
  return entity_load_single('opencrm_location_type', $id);
}

/**
 * Access callback for Location Types.
 */
function opencrm_location_type_access() {
  return user_access('administer opencrm_location types');
}

/**
 * Implements hook_menu_local_tasks_alter()
 */
function opencrm_location_menu_local_tasks_alter(&$data, $router_item, $root_path) {
  if ($router_item['path'] != 'admin/content/location') {
    return;
  }

  $bundles = db_select('opencrm_location_type', 't')
    ->fields('t', array('type', 'label'))
    ->condition('reusable', 1)
    ->execute()->fetchAllKeyed();

  foreach ($bundles as $type => $label) {
    $path = "location/add/{$type}";
    $title = t('Add new !label', array('!label' => $label));
    $data['actions']['output'][] = array(
      '#theme' => 'menu_local_task',
      '#link' => array(
        'title' => $title,
        'href' => $path,
        'localized_options' => array(
          'attributes' => array(
            'title' => $title,
          ),
        ),
      ),
    );
  }
  $data['actions']['count'] = count($data['actions']['output']);
}

/**
 * Implements hook_field_widget_info()
 */
function opencrm_location_field_widget_info() {
  $widgets = array();

  $widgets['opencrm_location_reference'] = array(
    'label' => t('Opencrm Location In-line Form (single)'),
    'field types' => array('entityreference'),
    'settings' => array(
      'fields' => array(),
      'type_settings' => array(),
    ),
    'behaviors' => array(
      'multiple values' => FIELD_BEHAVIOR_CUSTOM,
      'default value' => FIELD_BEHAVIOR_DEFAULT,
    ),
  );

  return $widgets;
}

/**
 * Implements hook_field_widget_form().
 */
function opencrm_location_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $settings = array(
    'entity_type' => 'opencrm_location',
    'column' => 'target_id',
  );

  // Collect bundles.
  if (!empty($field['settings']['handler_settings']['target_bundles'])) {
    $bundles = array_filter($field['settings']['handler_settings']['target_bundles']);
    if (!empty($bundles)) {
      $settings['bundles'] = array_values($bundles);
    }
  }
  else {
    $info = entity_get_info('opencrm_location');
    $settings['bundles'] = array_keys($info['bundles']);
  }

  // Build a parents array for this element's values in the form.
  $parents = array_merge($element['#field_parents'], array($element['#field_name'], $element['#language'], 0));

  // Assign a unique identifier to each widget.
  // Since $parents can get quite long, sha1() ensures that every id has
  // a consistent and relatively short length while maintaining uniqueness.
  $widget_id = sha1(implode('-', $parents));
  // Determine the wrapper id for the entire element
  $wrapper = 'opencrm-location-' . $widget_id;
  $element = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#description' => $instance['description'],
    '#prefix' => '<div id="'.$wrapper.'">',
    '#suffix' => '</div>',
    '#widget_id' => $widget_id,
    '#parents' => $parents,
  ) + $element;

  // Initialise an array in the form state.
  if (empty($form_state['opencrm_location_widget'][$widget_id])) {
    $form_state['opencrm_location_widget'][$widget_id] = array(
      'form' => NULL,
      'settings' => $settings,
      'instance' => $instance,
    );

    // Load the entity.
    $form_state['opencrm_location_widget'][$widget_id]['entity'] = array();
    if (!empty($items[0]['target_id'])) {
      $entity = entity_load_single('opencrm_location', $items[0]['target_id']);
      $form_state['opencrm_location_widget'][$widget_id]['entity'] = array(
        'entity' => $entity,
        'needs_save' => FALSE,
        'form' => NULL,
      );
    }
    else if (!empty($items[0]['entity'])) {
      $form_state['opencrm_location_widget'][$widget_id]['entity'] = array(
        'entity' => $items[0]['entity'],
        'needs_save' => FALSE,
        'form' => NULL,
      );
    }
  }
  $widget_state = &$form_state['opencrm_location_widget'][$widget_id];

  // Required behaviours:
  //   Selecting an existing location
  //   Creating a new location.
  // If a bundle is reusable then entities of that type can never be created
  // on the fly.
  // If a bundle is not marked as reusable then a new one is always created on
  // the fly.

  // If the bundle is not already evident the show an option to select the bundle.
  // Sniff out the bundle.
  $bundle = FALSE;
  $bundle_parents = $parents;
  array_push($bundle_parents, 'bundle');
  if (count($settings['bundles']) === 1) {
    $bundle = reset($settings['bundles']);
  }
  else if (!empty($form_state['input']) && drupal_array_get_nested_value($form_state['input'], $bundle_parents)) {
    $bundle = drupal_array_get_nested_value($form_state['input'], $bundle_parents);
  }
  else if (!empty($widget_state['entity']['entity'])) {
    $bundle = $widget_state['entity']['entity']->type;
  }
  else {
    $context = array(
      'form' => $form,
      'element' => $element,
      'field' => $field,
      'instance' => $instance,
      'langcode' => $langcode,
      'items' => $items,
      'delta' => $delta,
    );
    drupal_alter('opencrm_location_reference_field_widget_default_bundle', $bundle, $form_state, $context);
  }

  // If the bundle isn't known, select the bundle.
  $info = entity_get_info('opencrm_location');
  $options = array();
  foreach ($settings['bundles'] as $bundle_name) {
    $options[$bundle_name] = $info['bundles'][$bundle_name]['label'];
  }
  $element['bundle'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#title' => t('Which type of location do you require?'),
    '#widget_id' => $widget_id,
    '#empty_option' => t('- Please Select -'),
    '#default_value' => !empty($bundle) ? $bundle : NULL,
    '#ajax' => array(
      'callback' => 'opencrm_locaton_field_widget_ajax_get_element',
      'wrapper' => $wrapper,
    ),
  );

  // If we don't already have an entity create a raw entity.
  if (!empty($bundle)) {
    // Load the bundle.
    $bundle_obj = entity_load_single('opencrm_location_type', $bundle);
    if ($bundle_obj->reusable) {
      // Get the options.
      $options = db_select('opencrm_location', 'l')
        ->condition('type', $bundle)
        ->condition('status', 'active')
        ->fields('l', array('id', 'title'))
        ->execute()->fetchAllKeyed();
      if (!empty($widget_state['entity']['entity'])) {
        $options[$widget_state['entity']['entity']->id ?? '__current'] = $widget_state['entity']['entity']->label();
      }
      $element['target_id'] = array(
        '#type' => 'select',
        '#title' => $bundle_obj->label,
        '#options' => $options,
        '#default_value' => !empty($widget_state['entity']['entity']) && $widget_state['entity']['entity']->type == $bundle ?
          ($widget_state['entity']['entity']->id ?: '__current') : NULL,
        '#location_entity' => $widget_state['entity']['entity'] ?? NULL,
        '#element_validate' => ['opencrm_location_field_widget_process_validate'],
      );
    }
    else {
      if (empty($widget_state['entity']['entity']) || $widget_state['entity']['entity']->type != $bundle) {
        $widget_state['entity'] = array(
          'entity' => entity_create('opencrm_location', array('type' => $bundle)),
          'needs_save' => TRUE,
          'form' => NULL,
        );
      }

      // Do something else.
      field_attach_form('opencrm_location', $widget_state['entity']['entity'], $element, $form_state);
      $element['#element_validate'] = ['opencrm_location_field_widget_process_entity_form_validate'];
    }

    $widget_state['entity']['form'] = $element;
  }
  return $element;
}

/**
 * Validate the select list.
 *
 * @param array $element
 * @param array $form_state
 * @param array $form
 *
 * @return void
 */
function opencrm_location_field_widget_process_validate($element, &$form_state, $form) {
  $value = &drupal_array_get_nested_value($form_state['values'], $element['#parents']);
  if ($value === '__current') {
    if (!empty($element['#location_entity'])) {
      if (!entity_id('opencrm_location', $element['#location_entity'])) {
        $element['#location_entity']->save();
      }

      $value = entity_id('opencrm_location', $element['#location_entity']);
    }
    else {
      $value = NULL;
    }
  }
}

/**
 * Validate the embedded form.
 *
 * @param array $element
 * @param array $form_state
 * @param array $form
 *
 * @return void
 */
function opencrm_location_field_widget_process_entity_form_validate($element, &$form_state, $form) {
  $location = $element['#entity'];

  // Validate fields and if there were any validation errors do not proceed
  // further.
  field_attach_form_validate('opencrm_location', $location, $element, $form_state);
  if (form_get_errors()) {
    return;
  }

  // Update and save the location.
  field_attach_submit('opencrm_location', $location, $element, $form_state);
  $location->save();

  // Set the location id as the value of the form element.
  form_set_value($element, array('target_id' => $location->id), $form_state);
}

/**
 * Get the element when an ajax button is pressed.
 */
function opencrm_locaton_field_widget_ajax_get_element($form, $form_state) {
  $element = array();
  $array_parents = $form_state['triggering_element']['#array_parents'];
  // Remove the action and the actions container.
  $array_parents = array_slice($array_parents, 0, -2);
  $element = drupal_array_get_nested_value($form, $array_parents);
  return $element;
}

/**
 * Add the location field to relevant activities.
 *
 * @param PartyActivityType $type
 */
function opencrm_location_add_party_activity_field($type) {
  if (!field_info_field('activity_opencrm_location')) {
    $field = array(
      'field_name' => 'activity_opencrm_location',
      'type' => 'entityreference',
      'cardinality' => 1,
      'settings' => array(
        'target_type' => 'opencrm_location',
        'handler' => 'base',
        'handler_settings' => array(),
      ),
    );
    field_create_field($field);
  }

  if (!field_info_instance('party_activity', 'activity_opencrm_location', $type->type)) {
    $instance = array(
      'field_name' => 'activity_opencrm_location',
      'entity_type' => 'party_activity',
      'bundle' => $type->type,
      'label' => t('Location'),
      'required' => FALSE,
      'widget' => array(
        'type' => 'opencrm_location_reference',
      ),
    );
    field_create_instance($instance);
  }
}

/**
 * Implements hook_field_attach_create_bundle();
 */
function opencrm_location_field_attach_create_bundle($entity_type, $bundle) {
  // Only act on the right entity type.
  if ($entity_type != 'party_activity' || !module_exists('opencrm_activity')) {
    return;
  }

  // Only do anything if this bundle has a location.
  $type = entity_load_single('party_activity_type', $bundle);
  if (empty($type->data['has_opencrm_location'])) {
    return;
  }

  opencrm_location_add_party_activity_field($type);
}

/**
 * Implements hook_party_activity_type_update().
 *
 * If the location settings change change the field.
 */
function opencrm_location_party_activity_type_update($type) {
  if (empty($type->data['has_opencrm_location'])) {
    $instance = field_info_instance('party_activity', 'activity_opencrm_location', $type->type);
    if ($instance) {
      field_delete_instance($instance);
    }
  }
  else {
    opencrm_location_add_party_activity_field($type);
  }
}

/**
 * Implements hook_form_party_activity_type_form_alter().
 */
function opencrm_location_form_party_activity_type_form_alter(&$form, &$form_state) {
  $type = $form_state['party_activity_type'];

  $info = entity_get_info('party_activity');
  $label = $info['label'];
  $plural = !empty($info['plural label']) ? $info['plural label'] : "{$label}s";

  $form['opencrm_location'] = array(
    '#type' => 'fieldset',
    '#title' => t('Location Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => empty($type->data['has_opencrm_location']),
  );

  $form['opencrm_location']['has_opencrm_location'] = array(
    '#type' => 'checkbox',
    '#title' => t('@label of this type have a location', array('@label' => $plural)),
    '#default_value' => !empty($type->data['has_opencrm_location']),
    '#parents' => array('data', 'has_opencrm_location'),
  );
}

/**
 * Implements hook_opencrm_location_presave().
 */
function opencrm_location_opencrm_location_presave($location) {
  global $user;

  $location->changed = REQUEST_TIME;
  $location->changer = $user->party_attaching_party ?: NULL;

  if ($location->is_new) {
    $location->created = REQUEST_TIME;
    $location->creator = $user->party_attaching_party ?: NULL;
  }
}
