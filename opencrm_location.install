<?php
/**
 * @file
 * Install Process for the OpenCRM Locations Module.
 */

/**
 * Implements hook_schema().
 */
function opencrm_location_schema() {
  $schema = array();

  // OpenCRM Location Table - Stores Locations.
  $schema['opencrm_location'] = array(
    'description' => 'The base table for opencrm_location entities',
    'fields' => array(
      'id' => array(
        'description' => 'Primary Key: Identifier for a opencrm_location.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'type' => array(
        'description' => 'The machine-readable type of this opencrm_location.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'title' => array(
        'description' => 'The human-readable title of this opencrm_location.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the opencrm_location was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'creator' => array(
        'description' => 'The id of the party that created the location.',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the opencrm_location was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changer' => array(
        'description' => 'The id of the party that changed the location.',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'status' => array(
        'description' => 'Whether this location is available.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'type' => array('type'),
    ),
    'foreign keys' => array(
      'type' => array(
        'table' => 'opencrm_location_type',
        'columns' => array('type' => 'type'),
      ),
    ),
  );
  if (module_exists('uuid')) {
    $schema['opencrm_location']['fields']['uuid'] = uuid_schema_field_definition();
    $schema['opencrm_location']['indexes']['uuid'] = array('uuid');
  }

  // OpenCRM Location Type Table - Stores Bundles of OpenCRM Locations.
  $schema['opencrm_location_type'] = array(
    'description' => 'The base table for opencrm_location_type entities',
    'fields' => array(
      'id' => array(
        'description' => 'Primary Key: Identifier for a opencrm_location_type.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'type' => array(
        'description' => 'The machine-readable name of this opencrm_location_type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'The human-readable name of this opencrm_location_type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'reusable' => array(
        'description' => 'Whether locations of this type are re usable or not.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
      ),
      'status' => array(
        'type' => 'int',
        'not null' => TRUE,
        // Set the default to ENTITY_CUSTOM without using the constant as it is
        // not safe to use it at this point.
        'default' => 0x01,
        'size' => 'tiny',
        'description' => 'The exportable status of the entity.',
      ),
      'module' => array(
        'description' => 'The name of the providing module if the entity has been defined in code.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('id'),
    'unique keys' => array(
      'type' => array('type'),
    ),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function opencrm_location_install() {
  $t = get_t();

  // Create a basic event type.
  $type = entity_create('opencrm_location_type', array(
    'type' => 'opencrm_location',
    'label' => $t('Address (Default)'),
  ));
  $type->save();

  // Add an address field to the default location type.
  if (!field_info_field('location_address')) {
    $field = array(
      'field_name' => 'location_address',
      'cardinality' => 1,
      'type' => 'addressfield',
    );
    field_create_field($field);
  }
  if (!field_info_instance('opencrm_location', 'location_address', 'opencrm_location')) {
    $instance = array(
      'field_name' => 'location_address',
      'entity_type' => 'opencrm_location',
      'bundle' => 'opencrm_location',
      'label' => $t('Address'),
      'description' => $t("The address of this location."),
      'widget' => array(
        'type' => 'addressfield_standard',
        'settings' => array(
          'format_handlers' => array(
            'address' => 'address',
            'address-hide-country' => 'address-hide-country',
          ),
          'available_countries' => array(
            'GB' => 'GB',
            'US' => 'US',
          ),
        ),
      ),
    );
    field_create_instance($instance);
  }

  // Create a location geofield on the default location type.
  // Add the geofields to individuals and organisations.
  if (!field_info_field('location_geolocation')) {
    $field = array(
      'field_name' => 'location_geolocation',
      'type' => 'geofield',
      'cardinality' => 1,
    );
    field_create_field($field);
  }
  if (!field_info_instance('opencrm_location', 'location_geolocation', 'opencrm_location')) {
    $instance = array(
      'field_name' => 'location_geolocation',
      'entity_type' => 'opencrm_location',
      'bundle' => 'opencrm_location',
      'label' => $t('Geolocation'),
      'widget' => array(
        'type' => 'geocoder',
        'settings' => array(
          'geocoder_field' => 'location_address',
          'geocoder_handler' => 'google',
          'handler_settings' => array(
            'google' => array(
              'geometry_type' => 'point',
              'all_results' => 0,
            ),
          ),
        ),
      ),
      'display' => array(
        'default' => array(
          'type' => 'hidden',
        ),
      ),
    );
    field_create_instance($instance);
  }
}

/**
 * Add the geofield to all locations.
 */
function opencrm_location_update_7001() {
  if (!field_info_field('location_geolocation')) {
    $field = array(
      'field_name' => 'location_geolocation',
      'type' => 'geofield',
      'cardinality' => 1,
    );
    field_create_field($field);
  }

  if (!field_info_instance('opencrm_location', 'location_geolocation', 'opencrm_location')) {
    $instance = array(
      'field_name' => 'location_geolocation',
      'entity_type' => 'opencrm_location',
      'bundle' => 'opencrm_location',
      'label' => t('Geolocation'),
      'widget' => array(
        'type' => 'geocoder',
        'settings' => array(
          'geocoder_field' => 'location_address',
          'geocoder_handler' => 'google',
          'handler_settings' => array(
            'google' => array(
              'geometry_type' => 'point',
              'all_results' => 0,
            ),
          ),
        ),
      ),
      'display' => array(
        'default' => array(
          'type' => 'hidden',
        ),
      ),
    );
    field_create_instance($instance);
  }
}

/**
 * Add the uuid field.
 */
function opencrm_location_update_7002() {
  if (module_exists('uuid')) {
    db_add_field('opencrm_location', 'uuid', uuid_schema_field_definition());
    db_add_index('opencrm_location', 'uuid', array('uuid'));
  }
}

/**
 * Store the creater and changer of locations.
 */
function opencrm_location_update_7003() {
  foreach (array('creator', 'changer') as $field_name) {
    if (!db_field_exists('opencrm_location', $field_name)) {
      db_add_field('opencrm_location', $field_name, array(
        'description' => 'The id of the party that changed the location.',
        'type' => 'int',
        'not null' => FALSE,
      ));
    }
  }
}
