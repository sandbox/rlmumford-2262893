<?php
/**
 * @file
 *  Administration pages for OpenCRM Location.
 */

/**
 * UI Controller for the OpenCRM Location module.
 */
class OpenCRMLocationUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that
   * parent class hook_menu() is optimized for entity type administration.
   */
  public function hook_menu() {
    $items = array();
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

    $items['location/add/%opencrm_location_type'] =  array(
      'title callback' => 'opencrm_location_ui_page_title',
      'title arguments' => array('create', 2),
      'description' => t('Add an location.'),
      'page callback' => 'opencrm_location_add_form_wrapper',
      'page arguments' => array(2),
      'access callback' => 'opencrm_location_crud_access',
      'access arguments' => array('create', 2),
      'file path' => drupal_get_path('module', 'opencrm_location'),
      'file' => 'opencrm_location.admin.inc',
    );

    $items['location/%opencrm_location/edit'] = array(
      'title callback' => 'opencrm_location_ui_page_title',
      'title arguments' => array('edit', 1),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('opencrm_location_form', 1, 'edit'),
      'access callback' => 'opencrm_location_crud_access',
      'access arguments' => array('edit', 1),
      'file path' => drupal_get_path('module', 'opencrm_location'),
      'file' => 'opencrm_location.admin.inc',
    );

    return $items;
  }
}

/**
 * Title callback.
 *
 * @param string $op
 *   The operation being performed, one of 'create', 'edit', 'view'.
 * @param Entity $b
 *   In the case of 'create' the bundle of the location to be created,
 *   otherwise the location to be edited/viewed.
 */
function opencrm_location_ui_page_title($op, $b) {
  switch ($op) {
    case 'create':
      return t('Add new !type', array('!type' => $b->label));
    case 'edit':
      return t('Edit !title', array('!title' => $b->title));
    case 'view':
      return $b->title;
  }
}

/**
 * Form wrapper for the add page.
 */
function opencrm_location_add_form_wrapper($type) {
  $location = entity_create('opencrm_location', array(
    'type' => $type->type,
  ));

  return drupal_get_form('opencrm_location_form', $location, 'create');
}

/**
 * Form callback for editing and adding locations.
 */
function opencrm_location_form($form, &$form_state, $location, $op = 'edit') {

  $form['#location'] = $location;

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => !empty($location->title) ? $location->title : '',
    '#required' => TRUE,
  );

  field_attach_form('opencrm_location', $location, $form, $form_state);

  $form['status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#description' => t('If a location is not in use it cannot be used in new events.'),
    '#options' => array(
      'active' => t('In Use'),
      'inactive' => t('Not In Use'),
    ),
    '#default_value' => !empty($location->status) ? $location->status : 'active',
    '#required' => TRUE,
    '#weight' => 100,
  );

  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 500,
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => $op == 'create' ? t('Save') : t('Update'),
  );

  return $form;
}

/**
 * Submission Callback.
 */
function opencrm_location_form_submit($form, &$form_state) {
  $location = $form['#location'];
  $location->title = $form_state['values']['title'];
  $location->status = $form_state['values']['status'];

  field_attach_submit('opencrm_location', $location, $form, $form_state);
  $location->save();
}