<?php
/**
 * @file
 *   View hooks.
 */

/**
 * Implements hook_views_data_alter().
 */
function opencrm_location_views_data_alter(&$data) {
  $er_names = db_select('field_config', 'fc')
    ->condition('type', 'entityreference')
    ->fields('fc', array('field_name'))
    ->execute()
    ->fetchCol();

  foreach ($er_names as $field_name) {
    $field_info = field_info_field($field_name);
    if ($field_info['settings']['target_type'] != 'opencrm_location') {
      continue;
    }

    $table = 'field_data_'.$field_name;
    $column = $field_name.'_target_id';
    if (empty($data[$table]) || empty($data[$table][$column])) {
      continue;
    }
    if ($data[$table][$column]['filter']['handler'] != 'views_handler_filter_numeric') {
      continue;
    }

    $data[$table][$column]['filter']['handler'] = 'opencrm_location_handler_filter_opencrm_location';
  }

  //$data['opencrm_location']['changed']['field']['handler'] = 'views_handler_field_date';
  //$data['opencrm_location']['changed']['field']['click sortable'] = TRUE;
  $data['opencrm_location']['changer']['field']['handler'] = 'opencrm_location_views_handler_field_party';
  //$data['opencrm_location']['created']['field']['handler'] = 'views_handler_field_date';
  //$data['opencrm_location']['created']['field']['click sortable'] = TRUE;
  $data['opencrm_location']['creator']['field']['handler'] = 'opencrm_location_views_handler_field_party';
}
