<?php

/**
 * Special filter handler for opencrm locations.
 */
class opencrm_location_handler_filter_opencrm_location extends views_handler_filter_in_operator {

  function option_definition() {
    $options = parent::option_definition();
    $options['form_type'] = array('default' => 'reusable_select');
    $options['only_active'] = array('default' => FALSE);
    return $options;
  }

  function has_extra_options() {
    return TRUE;
  }

  function extra_options_form(&$form, &$form_state) {
    parent::extra_options_form($form, $form_state);
    $form['form_type'] = array(
      '#type' => 'radios',
      '#title' => t('Location Select Element'),
      '#default_value' => $this->options['form_type'],
      '#options' => array(
        'reusable_select' => t('Select List of Reusable Locations'),
        'autocomplete' => t('Autocomplete Widget'),
      ),
    );

    $form['only_active'] = array(
      '#type' => 'checkbox',
      '#title' => t('Only Show Active Locations'),
      '#description' => t('Check this box to exclude inactive reusable locations from the select list.'),
      '#default_value' => !empty($this->options['only_active']),
    );
  }

  function extra_options_validate($form, &$form_state) {
  }

  function value_form(&$form, &$form_state) {
    $form_type_method = $this->options['form_type'].'_value_form';
    $form['value'] = array();
    $form['value'] += call_user_func(array($this, $form_type_method));
  }

  function reusable_select_value_form() {
    $query = db_select('opencrm_location', 'ol');
    $query->leftJoin('opencrm_location_type', 'olt', 'olt.type = ol.type');
    $query->condition('olt.reusable', 1);
    if (!empty($this->options['only_active'])) {
      $query->condition('ol.status', 'active');
    }
    $query->addField('ol', 'id', 'id');
    $query->addField('ol', 'title', 'title');
    $query->addField('olt', 'label', 'type');
    $result = $query->execute();

    $options = array();
    while ($row = $result->fetchObject()) {
      $options[$row->type][$row->id] = $row->title;
    }

    $form = array(
      '#type' => 'select',
      '#options' => $options,
      '#multiple' => !empty($this->options['expose']['multiple']),
    );

    return $form;
  }

  function autocomplete_value_form() {
    $default = '';
    if ($this->value) {
      $field = field_info_field($this->definition['field_name']);
      $target_type = $field['settings']['target_type'];
      $entities = entity_load($target_type, is_array($this->value) ? $this->value : array($this->value));
      foreach ($entities as $entity) {
        if ($default) {
          $default .= ', ';
        }
        $default .= entity_label($target_type, $entity) . ' (' . entity_id($target_type, $entity) .')';
      }
    }

    $instances = field_read_instances(array('field_name' => $this->definition['field_name']));
    $instance = reset($instances);

    if ($this->options['expose']['multiple'] == 1) {
      $autocomplete_path = 'entityreference/autocomplete/tags/';
    }
    else {
      $autocomplete_path = 'entityreference/autocomplete/single/';
    }
    $autocomplete_path .= $this->definition['field_name'] . '/' . $instance['entity_type'] . '/' . $instance['bundle'] . '/NULL';
    $element = array(
      '#type' => 'textfield',
      '#default_vale' => $default,
      '#autocomplete_path' => $autocomplete_path,
      '#element_validate' => array('_entityreference_autocomplete_tags_validate'),
    );

    return $element;
  }

  function value_validate($form, &$form_state) {
    if ($this->options['form_type'] == 'autocomplete') {
      foreach ($form_state['values']['options']['value'] as $array) {
        $values[] = $array['target_id'];
      }

      $form_state['values']['options']['value'] = $values;
    }
  }
}
