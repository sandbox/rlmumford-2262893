<?php

/**
 * Handler for renderering a party label instead of an id.
 */
class opencrm_location_views_handler_field_party extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  function render($values) {
    $id = $this->get_value($values);
    $party = entity_load_single('party', $id);
    return entity_label('party', $party);
  }

}
