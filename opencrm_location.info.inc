<?php
/**
 * @file
 *  Alterations to the location entity property info.
 */

/**
 * Metadata Handler
 */
class OpenCRMLocationMetadataController extends EntityDefaultMetadataController {

  public static function statusOptionsList($name, $info) {
    return array(
      'active' => t('In Use'),
      'inactive' => t('Not in Use'),
    );
  }
}

/**
 * Implements hook_entity_property_info_alter().
 */
function opencrm_location_entity_property_info_alter(&$property_info) {
  $properties = &$property_info['opencrm_location']['properties'];

  // Update the status property
  $properties['status']['type'] = 'token';
  $properties['status']['options list'] = array('OpenCRMLocationMetadataController', 'statusOptionsList');

  // Update the opencrm location type.
  $properties['type']['required'] = TRUE;

  // Set the type of the creator/changer property.
  $properties['creator']['type'] = 'party';
  $properties['creator']['setter callback'] = 'entity_property_verbatim_set';
  $properties['changer']['type'] = 'party';
  $properties['changer']['setter callback'] = 'entity_property_verbatim_set';

  $properties['created']['type'] = 'date';
  $properties['changed']['type'] = 'date';
}
